#ifndef TERISBOTWITHFIXEDALGHORITM_H
#define TERISBOTWITHFIXEDALGHORITM_H

#include "tetrisBot.h"

class QPoint;


//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
struct TetrisBotWithFixedAlghoritm : TetrisBot
{
    virtual void findBestPlace(const Tetris &tetris, int timeLimit);
};

#endif
