#include "teachableTetrisBot.h"

#include "tetris.h"

#include <QFile>
#include <QDataStream>

constexpr int botDataFileFormatVersion = 14;
constexpr QDataStream::Version qDataStreamVersion = QDataStream::Qt_5_7;


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

//----------------------------------------------------------------------
QDataStream & operator<<(QDataStream &ds, const TeachableTetrisBot::StoredField &obj)
{
    ds << obj.field;
    ds << obj.placedFigurePos;
    ds << obj.placedFigureRotation;
    return ds;
}

//----------------------------------------------------------------------
QDataStream & operator>>(QDataStream &ds, TeachableTetrisBot::StoredField &obj)
{
    ds >> obj.field;
    ds >> obj.placedFigurePos;
    ds >> obj.placedFigureRotation;
    return ds;
}


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

//----------------------------------------------------------------------
bool isBotDataValid(const QVector<QList<TeachableTetrisBot::StoredField>> &storedFields)
{
    if(storedFields.size() < figuresCount)
        return false;

    for(const QList<TeachableTetrisBot::StoredField> &storedFieldOfFigure : storedFields)
    {
        for(const TeachableTetrisBot::StoredField &storedField : storedFieldOfFigure)
        {
            if(storedField.field.size() != Tetris::fieldWidth * Tetris::fieldHeight)
                return false;
        }
    }

    return true;
}


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

//----------------------------------------------------------------------
bool saveBotExperience(const TeachableTetrisBot &tetrisBot, const QString &filePath)
{
    QFile file(filePath);
    if(!file.open(QIODevice::WriteOnly))
        return false;

    QByteArray dataForSave;
    QDataStream ds(&dataForSave, QIODevice::WriteOnly);
    ds.setVersion(qDataStreamVersion);
    ds << botDataFileFormatVersion;
    ds << tetrisBot.storedFields;
    if(ds.status())
        return false;

    if(file.write(qCompress(dataForSave)) == -1)
        return false;

    return true;
}

//----------------------------------------------------------------------
bool loadBotExperience(TeachableTetrisBot &tetrisBot, const QString &filePath)
{
    QFile file(filePath);
    if(!file.open(QIODevice::ReadOnly))
        return false;

    QByteArray fileData = qUncompress(file.readAll());
    QDataStream ds(&fileData, QIODevice::ReadOnly);
    ds.setVersion(qDataStreamVersion);

    int format;
    ds >> format;
    if(ds.status() != QDataStream::Ok || format != botDataFileFormatVersion)
        return false;

    QVector<QList<TeachableTetrisBot::StoredField>> storedFieldParts;
    ds >> storedFieldParts;

    if(ds.status() != QDataStream::Ok)
        return false;

    if(!isBotDataValid(storedFieldParts))
        return false;

    tetrisBot.storedFields = storedFieldParts;

    return true;
}
