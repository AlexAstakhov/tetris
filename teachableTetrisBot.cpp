#include "teachableTetrisBot.h"

#include "tetris.h"

#include <QRect>
#include <QElapsedTimer>


QBitArray fieldAsBitArray(const int field[Tetris::fieldWidth][Tetris::fieldHeight]);
QBitArray fieldPart(const QRect &fieldPartRect, const QBitArray &field);
int sameBitsCount(const QBitArray &field1, const QBitArray &field2);
bool operator==(const TeachableTetrisBot::StoredField &a, const TeachableTetrisBot::StoredField &b);


//----------------------------------------------------------------------
void TeachableTetrisBot::teach(const Tetris &tetris)
{
    if(!tetris.isFigurePlaced)
        return;

    StoredField fieldForStore;
    fieldForStore.field = fieldAsBitArray(tetris.field);
    fieldForStore.placedFigurePos = tetris.figurePos;
    fieldForStore.placedFigureRotation = tetris.figureRotation;

    int indexOfExist = storedFields[tetris.figureIndex].indexOf(fieldForStore);
    if(indexOfExist < 0)
        storedFields[tetris.figureIndex] << fieldForStore;
    else
        storedFields[tetris.figureIndex][indexOfExist] = fieldForStore;
}

//----------------------------------------------------------------------
void TeachableTetrisBot::findBestPlace(const Tetris &tetris, int timeLimit)
{
    if(tetris.isFirstFigureStep)
    {
        bestPos = QPoint();
        bestRotation = 0;
        maxSameBitsCountOfPart = 0;
        checkingRotation = 0;
        bestVariant.storedField = 0;
        checkingX = 1;
        checkingY = Tetris::fieldHeight - 1;
        curFullField = fieldAsBitArray(tetris.field);
    }

    QElapsedTimer elapsed;
    elapsed.start();

    while(checkingRotation < figuresRotationCount)
    {
        while(checkingY >= 0)
        {
            while(checkingX < Tetris::fieldWidth)
            {
                checkRect(tetris);

                ++checkingX;

                if(elapsed.elapsed() > timeLimit)
                    goto exit;
            }

            checkingX = 1;
            --checkingY;
        }

        checkingY = Tetris::fieldHeight - 1;
        ++checkingRotation;
    }

    exit:

    if(bestVariant.storedField)
    {
        bestPos = bestVariant.storedField->placedFigurePos - (bestVariant.storedFieldPartRect.topLeft() - bestVariant.checkingFieldPartRect.topLeft());
        bestRotation = bestVariant.storedField->placedFigureRotation;
    }
}

//----------------------------------------------------------------------
void TeachableTetrisBot::checkRect(const Tetris &tetris)
{
    QRect figureRect = boundedFigureRect(tetris.figureIndex, checkingRotation);

    for(const StoredField &storedField : storedFields.at(tetris.figureIndex))
    {
        if(checkingRotation != storedField.placedFigureRotation)
            continue;

        QRect checkingFieldPartRect = figureRect;
        checkingFieldPartRect.moveLeft(checkingX);
        checkingFieldPartRect.moveTop(checkingY);
        if(checkingFieldPartRect.right() >= Tetris::fieldWidth || checkingFieldPartRect.bottom() >= Tetris::fieldHeight)
            continue;

        QRect storedFieldPartRect = figureRect;
        storedFieldPartRect.moveLeft(storedField.placedFigurePos.x() + figureRect.left());
        storedFieldPartRect.moveTop(storedField.placedFigurePos.y() + figureRect.top());
        if(storedFieldPartRect.top() < 0)
            continue;

        if(!increaseRects(checkingFieldPartRect, storedFieldPartRect))
            continue;

        QBitArray checkingFieldPart = fieldPart(checkingFieldPartRect, curFullField);
        QBitArray storedFieldPart = fieldPart(storedFieldPartRect, storedField.field);
        int sameBitsCountOfPart = sameBitsCount(storedFieldPart, checkingFieldPart);
        if(sameBitsCountOfPart > maxSameBitsCountOfPart)
        {
            maxSameBitsCountOfPart = sameBitsCountOfPart;
            bestVariant.checkingFieldPartRect = checkingFieldPartRect;
            bestVariant.storedField = &storedField;
            bestVariant.storedFieldPartRect = storedFieldPartRect;
        }
    }
}

//----------------------------------------------------------------------
bool TeachableTetrisBot::increaseRects(QRect &checkingFieldPartRect, QRect &storedFieldPartRect)
{
    bool rectIncreased = false;

    if(checkingFieldPartRect.left() > 0 && storedFieldPartRect.left() > 0)
    {
        checkingFieldPartRect.setLeft(checkingFieldPartRect.left() - 1);
        storedFieldPartRect.setLeft(storedFieldPartRect.left() - 1);
        rectIncreased = true;
    }

    if(checkingFieldPartRect.right() < Tetris::fieldWidth - 1 && storedFieldPartRect.right() < Tetris::fieldWidth - 1)
    {
        checkingFieldPartRect.setRight(checkingFieldPartRect.right() + 1);
        storedFieldPartRect.setRight(storedFieldPartRect.right() + 1);
        rectIncreased = true;
    }

    if(checkingFieldPartRect.top() > 0 && storedFieldPartRect.top() > 0)
    {
        checkingFieldPartRect.setTop(checkingFieldPartRect.top() - 1);
        storedFieldPartRect.setTop(storedFieldPartRect.top() - 1);
        rectIncreased = true;
    }

    if(checkingFieldPartRect.bottom() < Tetris::fieldHeight - 1 && storedFieldPartRect.bottom() < Tetris::fieldHeight - 1)
    {
        checkingFieldPartRect.setBottom(checkingFieldPartRect.bottom() + 1);
        storedFieldPartRect.setBottom(storedFieldPartRect.bottom() + 1);
        rectIncreased = true;
    }

    return rectIncreased;
}

//----------------------------------------------------------------------
void TeachableTetrisBot::clearExperiance()
{
    for(QList<StoredField> &variantsForFigure : storedFields)
    {
        variantsForFigure.clear();
    }
}

//----------------------------------------------------------------------
inline QBitArray fieldAsBitArray(const int field[Tetris::fieldWidth][Tetris::fieldHeight])
{
    QBitArray result(Tetris::fieldWidth * Tetris::fieldHeight);
    for(int x = 0; x < Tetris::fieldWidth; ++x)
    {
        for(int y = 0; y < Tetris::fieldHeight; ++y)
        {
            result[x + y * Tetris::fieldWidth] = (bool)field[x][y];
        }
    }

    return result;
}

//----------------------------------------------------------------------
inline QBitArray fieldPart(const QRect &fieldPartRect, const QBitArray &field)
{
    QBitArray result(fieldPartRect.width() * fieldPartRect.height());
    for(int x = fieldPartRect.left(); x <= fieldPartRect.right(); ++x)
    {
        for(int y = fieldPartRect.top(); y <= fieldPartRect.bottom(); ++y)
        {
            int index = (x - fieldPartRect.left()) + (y - fieldPartRect.top()) * fieldPartRect.width();
            result[index] = field.at(x + y * Tetris::fieldWidth);
        }
    }

    return result;
}

//----------------------------------------------------------------------
inline int sameBitsCount(const QBitArray &field1, const QBitArray &field2)
{
    int curSameBits = 0;
    int fieldsSize = qMin(field1.size(), field2.size());
    for(int i = 0; i < fieldsSize; ++i)
    {
        if(field1.at(i) == field2.at(i))
            ++curSameBits;
    }

    return curSameBits;
}

//--------------------------------------------------------------
inline bool operator==(const TeachableTetrisBot::StoredField &a, const TeachableTetrisBot::StoredField &b)
{
    return a.field == b.field && a.placedFigurePos == b.placedFigurePos && a.placedFigureRotation == b.placedFigureRotation;
}
