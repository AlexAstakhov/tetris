#include "figures.h"

#include <QRect>


//-----------------------------------------------------------------
QRect boundingRects[figuresCount][figuresRotationCount];
bool calcBoundedRects()
{
    for(int figureIndex = 0; figureIndex < figuresCount; ++figureIndex)
    {
        for(int rotation = 0; rotation < figuresRotationCount; ++rotation)
        {
            QRect result;

            for(int x = 0; x < figuresWidth; ++x)
            {
                for(int y = 0; y < figuresHeight; ++y)
                {
                    if(figures[figureIndex][rotation][x][y])
                    {
                        if(!result.isNull())
                        {
                            if(x < result.left())
                                result.setLeft(x);
                            else if(x > result.right())
                                result.setRight(x);

                            if(y < result.top())
                                result.setTop(y);
                            else if(y > result.bottom())
                                result.setBottom(y);
                        }
                        else
                        {
                            result.setCoords(x, y, x, y);
                        }
                    }
                }
            }

            boundingRects[figureIndex][rotation] = result;
        }
    }

    return true;
}
static bool tmp = calcBoundedRects();


//-----------------------------------------------------------------
QRect boundedFigureRect(int figureIndex, int rotation)
{
    return boundingRects[figureIndex][rotation];
}
