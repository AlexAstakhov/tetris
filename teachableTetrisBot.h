#ifndef TEACHABLETETRISBOT_H
#define TEACHABLETETRISBOT_H

#include <QList>
#include <QVector>
#include <QPoint>
#include <QBitArray>

#include "figures.h"
#include "tetrisBot.h"


//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
struct TeachableTetrisBot : TetrisBot
{
    void teach(const Tetris &tetris);
    virtual void findBestPlace(const Tetris &tetris, int time);

    struct StoredField
    {
        QBitArray field;
        QPoint placedFigurePos;
        int placedFigureRotation;
    };

    QVector/*for each figure*/<QList<StoredField>> storedFields = QVector<QList<StoredField>>(figuresCount);

    void clearExperiance();

    void checkRect(const Tetris &tetris);
    bool increaseRects(QRect &checkingFieldPartRect, QRect &storedFieldPartRect);

    struct BestVariant
    {
        const StoredField *storedField = 0;
        QRect storedFieldPartRect;
        QRect checkingFieldPartRect;
    };
    BestVariant bestVariant;

    int maxSameBitsCountOfPart = 0;
    int checkingRotation = figuresRotationCount;
    int checkingX = 0;
    int checkingY = -1;
    QBitArray curFullField;
};

#endif
