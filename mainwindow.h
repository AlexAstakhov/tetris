#ifndef MAINWINDOW_H
#define MAINWINDOW_H

struct Tetris;
struct TeachableTetrisBot;
struct TetrisBotWithFixedAlghoritm;

#include "ui_mainwindow.h"

#include <QMainWindow>
#include <QElapsedTimer>


//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    virtual void paintEvent(QPaintEvent *);
    virtual void resizeEvent(QResizeEvent *);
    virtual void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    bool eventFilter(QObject *target, QEvent *event);

private slots:
    void step();

private:
    Ui::MainWindow ui;

    Tetris &tetris1;
    TetrisBotWithFixedAlghoritm &tetrisBotWithFixedAlghoritm;
    Tetris &tetris2;
    TeachableTetrisBot &teachableTetrisBot;

    QElapsedTimer elapsedFromLastStep;

    void setUserControlVisible(bool visible);

    void drawTetris(const Tetris &tetris, float brickSize, float whereDrawX, float whereDrawY);
};

#endif // MAINWINDOW_H
