#include "tetrisBotWithFixedAlghoritm.h"

#include "tetris.h"

#include <QRect>
#include <QElapsedTimer>


int toTopOfFigure(const Tetris &tetris, int rotation);
int voidsUnder(const Tetris &tetris, const QPoint &figurePos, int rotation);
int fullLinesCount(const Tetris &tetris, const QPoint &figurePos, int rotation);


//----------------------------------------------------------------
void TetrisBotWithFixedAlghoritm::findBestPlace(const Tetris &tetris, int timeLimit)
{
    if(!tetris.isFirstFigureStep)
        return;

    QElapsedTimer elapsed;
    elapsed.start();

    bestPos = QPoint();
    bestRotation = tetris.figureRotation;

    int minVoidsAfterPlacing = figuresHeight;
    int highestY = tetris.figurePos.y();

    // try all rotation variants of figure
    for(int rotation = 0; rotation < figuresRotationCount; ++rotation)
    {
        // try of all places by x
        QRect figureRect = boundedFigureRect(tetris.figureIndex, rotation);
        for(int x = -figureRect.left(); x < Tetris::fieldWidth - 1 - figureRect.width(); ++x)
        {
            if(elapsed.elapsed() > timeLimit)
                return;

            // try go down
            for(int y = tetris.figurePos.y(); y < Tetris::fieldHeight - 1; ++y)
            {
                if(tetris.isCollide(QPoint(x, y), rotation))
                {
                    QPoint posBeforeCollision(x, y - 1);

                    int deletedLinesAfterPlacing = fullLinesCount(tetris, posBeforeCollision, rotation);
                    int voidsBelowFigure = voidsUnder(tetris, posBeforeCollision, rotation);
                    int topBeforeCollision = posBeforeCollision.y() + toTopOfFigure(tetris, rotation);

                    if(deletedLinesAfterPlacing >= figureRect.height())
                    {
                        bestPos = posBeforeCollision;
                        bestRotation = rotation;
                        return;
                    }
                    else if(voidsBelowFigure < minVoidsAfterPlacing ||
                           (voidsBelowFigure == minVoidsAfterPlacing && topBeforeCollision > highestY))
                    {
                        minVoidsAfterPlacing = voidsBelowFigure;
                        highestY = topBeforeCollision;
                        bestPos = posBeforeCollision;
                        bestRotation = rotation;
                    }

                    break;
                }
            }
        }
    }
}

//----------------------------------------------------------------
int toTopOfFigure(const Tetris &tetris, int rotation)
{
    for(int y = 0; y < figuresHeight; ++y)
    {
        for(int x = 0; x < figuresWidth; ++x)
        {
            if(figures[tetris.figureIndex][rotation][x][y])
                return y;
        }
    }

    return 0;
}

//----------------------------------------------------------------
int voidsUnder(const Tetris &tetris, const QPoint &figurePos, int rotation)
{
    int voids = 0;

    QRect figureRectOnField = tetris.boundedByFieldBoundedRectByFigure(tetris.figureIndex, figurePos, rotation);
    for(int x = figureRectOnField.left(); x <= figureRectOnField.right(); ++x)
    {
        for(int y = figureRectOnField.bottom(); y >= figureRectOnField.top(); --y)
        {
            if(figures[tetris.figureIndex][rotation][x - figurePos.x()][y - figurePos.y()])
                break;

            if(!tetris.field[x][y])
                ++voids;
        }

        int y = figureRectOnField.bottom() + 1;
        if(!tetris.field[x][y])
            ++voids;
    }

    return voids;
}

//----------------------------------------------------------------
int fullLinesCount(const Tetris &tetris, const QPoint &figurePos, int rotation)
{
    int fullLinesCount = 0;

    int tmpField[Tetris::fieldWidth][Tetris::fieldHeight];
    memcpy(tmpField, tetris.field, sizeof(tmpField));
    Tetris::writeFigureToField(tmpField, tetris.figureIndex, figurePos, rotation, 1);

    for(int y = Tetris::fieldHeight - 2; y >= 0; --y)
    {
        int x = 1;
        for(; x < Tetris::fieldWidth - 1; ++x)
        {
            if(!tmpField[x][y])
                break;
        }

        //if line is full
        if(x >= Tetris::fieldWidth - 1)
            ++fullLinesCount;
    }

    return fullLinesCount;
}
