#include "mainwindow.h"

#include <QApplication>
#include <QScopedPointer>
#include <time.h>


//----------------------------------------------------------------------
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    srand(time(0));
    QScopedPointer<MainWindow> w(new MainWindow);
    w->show();
    return a.exec();
}
