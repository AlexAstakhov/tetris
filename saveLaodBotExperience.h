#ifndef SAVELAODBOTEXPERIENCE_H
#define SAVELAODBOTEXPERIENCE_H

struct TeachableTetrisBot;
class QString;


bool saveBotExperience(const TeachableTetrisBot &tetrisBot, const QString &filePath);
bool loadBotExperience(TeachableTetrisBot &tetrisBot, const QString &filePath);

#endif // SAVELAODBOTEXPERIENCE_H
