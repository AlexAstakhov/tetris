QT += core gui
QT += widgets

TARGET = tetris
TEMPLATE = app

CONFIG += c++11
QMAKE_CXXFLAGS += -std=c++11
QMAKE_CXXFLAGS += -ffast-math
QMAKE_CXXFLAGS += -funroll-loops
QMAKE_CXXFLAGS += -fomit-frame-pointer
QMAKE_CXXFLAGS += -fno-math-errno
QMAKE_CXXFLAGS += -fno-exceptions
QMAKE_CXXFLAGS += -fno-rtti

Release:QMAKE_CXXFLAGS += -flto
Release:QMAKE_CXXFLAGS += -O3


SOURCES += \
    main.cpp\
    mainwindow.cpp \
    tetris.cpp \
    saveLaodBotExperience.cpp \
    teachableTetrisBot.cpp \
    tetrisBotWithFixedAlghoritm.cpp \
    figures.cpp \
    tetrisBot.cpp

HEADERS += \
    mainwindow.h \
    tetris.h \
    figures.h \
    saveLaodBotExperience.h \
    teachableTetrisBot.h \
    tetrisBotWithFixedAlghoritm.h \
    tetrisBot.h

FORMS += mainwindow.ui
