#include "tetrisBot.h"

#include "tetris.h"


Action TetrisBot::play(const Tetris &tetris)
{
    findBestPlace(tetris, 12);

    if(bestPos.isNull())
        return Action::Drop;

    if(tetris.figureRotation != bestRotation)
        return Action::Rotate;
    else if(tetris.figurePos.x() < bestPos.x())
        return Action::MoveRight;
    else if(tetris.figurePos.x() > bestPos.x())
        return Action::MoveLeft;
    else
        return Action::Drop;
}
