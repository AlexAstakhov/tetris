#ifndef TETRISBOT_H
#define TETRISBOT_H

struct Tetris;
enum struct Action : char;

#include <QPoint>


//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
struct TetrisBot
{
    Action play(const Tetris &tetris);
    virtual void findBestPlace(const Tetris &tetris, int time) = 0;

    QPoint bestPos;
    int bestRotation = 0;

    virtual ~TetrisBot() {}
};

#endif // TETRISBOT_H
