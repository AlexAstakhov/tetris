#include "mainwindow.h"

#include "tetris.h"
#include "teachableTetrisBot.h"
#include "tetrisBotWithFixedAlghoritm.h"
#include "saveLaodBotExperience.h"

#include <QPainter>
#include <QKeyEvent>
#include <QTimer>
#include <QStandardPaths>
#include <QDebug>
#include <QApplication>
#include <QScreen>

const QString experienceFilePath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "tetrisBotExperience";


//----------------------------------------------------------------------
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , tetris1(*(new Tetris))
    , tetrisBotWithFixedAlghoritm(*(new TetrisBotWithFixedAlghoritm()))
    , tetris2(*(new Tetris))
    , teachableTetrisBot(*(new TeachableTetrisBot()))
{
    ui.setupUi(this);

    connect(ui.teachButton, &QPushButton::clicked, [&]()
    {
        if(ui.teachButton->isChecked())
        {
            ui.autoTeachButton->setChecked(false);
            setUserControlVisible(true);
        }
    });

    connect(ui.autoTeachButton, &QPushButton::clicked, [&]()
    {
        if(ui.autoTeachButton->isChecked())
        {
            ui.teachButton->setChecked(false);
            setUserControlVisible(false);
        }
    });

    connect(ui.restartButton, &QPushButton::clicked, [&]()
    {
        tetris1.restart();
        tetris2.restart();
    });

    connect(ui.clearExperianceButton, &QPushButton::clicked, [&]()
    {
        teachableTetrisBot.clearExperiance();
    });

    // user control buttons

    connect(ui.leftButton, &QPushButton::pressed, [&]()
    {
        tetris1.action = Action::MoveLeft;
    });

    connect(ui.rightButton, &QPushButton::pressed, [&]()
    {
        tetris1.action = Action::MoveRight;
    });

    connect(ui.rotateButton, &QPushButton::pressed, [&]()
    {
        tetris1.action = Action::Rotate;
    });

    connect(ui.dropButton, &QPushButton::pressed, [&]()
    {
        tetris1.action = Action::Drop;
    });

    auto relesed = [&]()
    {
        tetris1.action = Action::Nothing;
    };

    connect(ui.leftButton, &QPushButton::released, relesed);
    connect(ui.rightButton, &QPushButton::released, relesed);
    connect(ui.rotateButton, &QPushButton::released, relesed);
    connect(ui.dropButton, &QPushButton::released, relesed);

    ui.teachButton->installEventFilter(this);
    ui.autoTeachButton->installEventFilter(this);
    ui.restartButton->installEventFilter(this);
    ui.clearExperianceButton->installEventFilter(this);

    ui.leftButton->installEventFilter(this);
    ui.rightButton->installEventFilter(this);
    ui.rotateButton->installEventFilter(this);
    ui.dropButton->installEventFilter(this);

    if(!loadBotExperience(teachableTetrisBot, experienceFilePath))
        qDebug() << "can't load bot experience";

    setUserControlVisible(ui.teachButton->isChecked());

    elapsedFromLastStep.restart();
    step();
}

//----------------------------------------------------------------------
MainWindow::~MainWindow()
{
    if(!saveBotExperience(teachableTetrisBot, experienceFilePath))
        qDebug() << "can't save bot experience";

    delete &tetris1;
    delete &tetrisBotWithFixedAlghoritm;
    delete &tetris2;
    delete &teachableTetrisBot;
}

//----------------------------------------------------------------------
void MainWindow::step()
{
    tetris2.action = teachableTetrisBot.play(tetris2);

    if(!ui.teachButton->isChecked())
        tetris1.action = tetrisBotWithFixedAlghoritm.play(tetris1);

    teachableTetrisBot.teach(tetris1);

    tetris1.step();
    tetris2.step();

    repaint();

    int msecsToNextStep = qMax((qint64)0, 16 - elapsedFromLastStep.elapsed());
    elapsedFromLastStep.restart();
    QTimer::singleShot(msecsToNextStep, this, &MainWindow::step);
}


//----------------------------------------------------------------------
void MainWindow::drawTetris(const Tetris &tetris, float brickSize, float whereDrawX, float whereDrawY)
{
    QPainter painter(this);
    painter.setPen(QColor(0, 100, 80));
    painter.setRenderHint(QPainter::HighQualityAntialiasing);

    // draw field
    for(int x = 0; x < tetris.fieldWidth; ++x)
    {
        for(int y = 0; y < tetris.fieldHeight; ++y)
        {
            painter.setBrush(QColor(tetris.field[x][y]));
            painter.drawRect(whereDrawX + x * brickSize, whereDrawY + y * brickSize, brickSize, brickSize);
        }
    }

    // draw figure
    for(int x = 0; x < figuresWidth; ++x)
    {
        for(int y = 0; y < figuresHeight; ++y)
        {
            if(!figures[tetris.figureIndex][tetris.figureRotation][x][y])
                continue;

            painter.setBrush(tetris.figureColor);
            painter.drawRect(whereDrawX + x * brickSize + tetris.figurePos.x() * brickSize, whereDrawY + y * brickSize + tetris.figurePos.y() * brickSize, brickSize, brickSize);
        }
    }

    // draw score
    painter.setPen(QColor(0, 200, 200));
    painter.drawText(whereDrawX + brickSize * 1.4, whereDrawY + brickSize * 1.4, "Score: " + QString::number(tetris.score));
}


//----------------------------------------------------------------------
void MainWindow::paintEvent(QPaintEvent *)
{
    QRect windowRect = rect();
    float brickSize = qMin((float)windowRect.width() / 2 / Tetris::fieldWidth, (float)windowRect.height() / Tetris::fieldHeight);
    drawTetris(tetris1, brickSize, windowRect.left(), windowRect.bottom() - brickSize * Tetris::fieldHeight);
    drawTetris(tetris2, brickSize, brickSize * Tetris::fieldWidth, windowRect.bottom() - brickSize * Tetris::fieldHeight);
}

//----------------------------------------------------------------------
void MainWindow::resizeEvent(QResizeEvent *)
{
    QScreen *screen = QApplication::primaryScreen();
    float sizeX = screen->physicalDotsPerInchX() / 1.5;
    float sizeY = screen->physicalDotsPerInchY() / 1.5;
    QSize controlButtonsSize(sizeX, sizeY);
    ui.leftButton->setFixedSize(controlButtonsSize);
    ui.rightButton->setFixedSize(controlButtonsSize);
    ui.rotateButton->setFixedSize(controlButtonsSize);
    ui.dropButton->setFixedSize(controlButtonsSize);
}

//----------------------------------------------------------------------
bool MainWindow::eventFilter(QObject *object, QEvent *event)
{
    if(QPushButton *button = qobject_cast<QPushButton*>(object))
    {
        if(event->type() == QEvent::Paint)
        {
            QPainter painter(button);
            painter.setRenderHints(QPainter::HighQualityAntialiasing);
            QPen pen(QColor(180, 100, 255, 120));
            pen.setWidth(5);
            painter.setPen(pen);
            QRect buttonRect = button->rect().adjusted(0, 0, -1, -1);
            QLinearGradient lgrad(buttonRect.topLeft(), buttonRect.bottomLeft());
            bool buttonIsDown = button->isDown();
            if(!buttonIsDown)
            {
                lgrad.setColorAt(0.0, QColor(0, 0, 200, 50));
                lgrad.setColorAt(1.0, QColor(80, 100, 0, 100));
            }
            else
            {
                lgrad.setColorAt(0.0, QColor(0, 0, 200, 150));
                lgrad.setColorAt(1.0, QColor(80, 100, 0, 200));
            }
            painter.setBrush(lgrad);
            painter.drawRoundedRect(buttonRect, 7, 7);

            if(button->isChecked() || buttonIsDown)
                pen.setColor(QColor(30, 255, 200, 230));
            else
                pen.setColor(QColor(255, 255, 100, 230));

            pen.setWidth(5);
            painter.setPen(pen);
            painter.drawText(buttonRect, button->text(), QTextOption(Qt::AlignHCenter | Qt::AlignVCenter));
            return true;
        }
    }

    return QMainWindow::eventFilter(object, event);
}

//----------------------------------------------------------------------
void MainWindow::keyPressEvent(QKeyEvent *event)
{
    int key = event->key();

    if(ui.teachButton->isChecked())
    {
        switch(key)
        {
        case Qt::Key_Up:
            tetris1.action = Action::Rotate;
            break;
        case Qt::Key_Left:
            tetris1.action = Action::MoveLeft;
            break;
        case Qt::Key_Right:
            tetris1.action = Action::MoveRight;
            break;
        case Qt::Key_Down:
            tetris1.action = Action::Drop;
            break;
        default:
            break;
        };
    }

    if(key == Qt::Key_Back || key == Qt::Key_Escape)
        close();
}

//----------------------------------------------------------------------
void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    if(!ui.teachButton->isChecked())
        return;

    if(event->key() == Qt::Key_Up || event->key() == Qt::Key_Left || event->key() == Qt::Key_Right || event->key() == Qt::Key_Down)
        tetris1.action = Action::Nothing;
}

//----------------------------------------------------------------------
void MainWindow::setUserControlVisible(bool visible)
{
    ui.leftButton->setVisible(visible);
    ui.rightButton->setVisible(visible);
    ui.rotateButton->setVisible(visible);
    ui.dropButton->setVisible(visible);
}
