#include "tetris.h"

#include <stdlib.h>
#include <QRect>


//----------------------------------------------------------------------
Tetris::Tetris()
{
    constexpr int wall = qRgb(15, 35, 110);

    // create left and right walls
    for(int y = 0; y < fieldHeight; ++y)
    {
        field[0][y] = wall;
        field[fieldWidth - 1][y] = wall;
    }

    // create bottom wall
    for(int x = 1; x < fieldWidth - 1; ++x)
    {
        field[x][fieldHeight - 1] = wall;
    }

    restart();
}

//----------------------------------------------------------------------
void Tetris::restart()
{
    clearField();
    makeNewFigure();
    score = 0;
}

//----------------------------------------------------------------------
void Tetris::step()
{
    if(isFigurePlaced)
    {
        if(!isFirstFigureStep)
        {
            writeFigureToField(field, figureIndex, figurePos, figureRotation, figureColor.rgb());
            deleteFullLines();
            makeNewFigure();
        }
        else
        {
            // if figure can't move then game over
            restart();
            return;
        }
    }
    else
    {
        isFirstFigureStep = false;
    }

    QPoint newFigurePos = figurePos;

    ++skippedFallings;
    if(skippedFallings >= skipFallings || action == Action::Drop)
    {
        skippedFallings = 0;
        newFigurePos.setY(figurePos.y() + 1);
    }

    if(action == Action::MoveLeft)
    {
        if(!isCollide(QPoint(figurePos.x() - 1, newFigurePos.y()), figureRotation))
            newFigurePos.setX(figurePos.x() - 1);
    }
    else if(action == Action::MoveRight)
    {
        if(!isCollide(QPoint(figurePos.x() + 1, newFigurePos.y()), figureRotation))
            newFigurePos.setX(figurePos.x() + 1);
    }
    else if(action == Action::Rotate)
    {
        int oldRotation = figureRotation;
        ++figureRotation;
        if(figureRotation >= figuresRotationCount)
            figureRotation = 0;

        if(isCollide(newFigurePos, figureRotation))
            figureRotation = oldRotation;
    }

    // is figure placed?
    if(isCollide(newFigurePos, figureRotation))
        isFigurePlaced = true;
    else
        figurePos = newFigurePos;

    if(action != Action::Drop)
        action = Action::Nothing;
}

//----------------------------------------------------------------------
void Tetris::clearField()
{
    for(int x = 1; x < fieldWidth - 1; ++x)
    {
        for(int y = 0; y < fieldHeight - 1; ++y)
        {
            field[x][y] = 0;
        }
    }
}

//----------------------------------------------------------------------
void Tetris::makeNewFigure()
{
    figureIndex = rand() % figuresCount;
    figureRotation = 0;

    figurePos.setX(fieldWidth / 2 - figuresWidth / 2);
    figurePos.setY(-boundedFigureRect(figureIndex, figureRotation).bottom() + 1);

    figureColor = QColor(70 + rand() % 185, 70 + rand() % 185, 70 + rand() % 185);

    action = Action::Nothing;
    isFirstFigureStep = true;
    isFigurePlaced = false;
}

//----------------------------------------------------------------------
bool Tetris::isCollide(const QPoint &pos, int rotation) const
{
    QRect figureRectOnField = boundedByFieldBoundedRectByFigure(figureIndex, pos, rotation);
    for(int x = figureRectOnField.left(); x <= figureRectOnField.right(); ++x)
    {
        for(int y = figureRectOnField.top(); y <= figureRectOnField.bottom(); ++y)
        {
            if(field[x][y])
            {
                if(figures[figureIndex][rotation][x - pos.x()][y - pos.y()])
                    return true;
            }
        }
    }

    return false;
}

//----------------------------------------------------------------------
void Tetris::deleteFullLines()
{
    for(int y = fieldHeight - 2; y >= 0; --y)
    {
        int x = 1;
        for(; x < fieldWidth - 1; ++x)
        {
            if(!field[x][y])
                break;
        }

        // if line is full
        if(x >= fieldWidth - 1)
        {
            ++score;

            for(int y2 = y; y2 >= 0; --y2)
            {
                for(int x = 1; x < fieldWidth - 1; ++x)
                {
                    if(y2 > 0)
                        field[x][y2] = field[x][y2 - 1];
                    else
                        field[x][y2] = 0;
                }
            }

            ++y;
        }
    }
}

//----------------------------------------------------------------------
void Tetris::writeFigureToField(int field[fieldWidth][fieldHeight], int figureIndex, const QPoint &figurePos, int figureRotation, int figureColor)
{
    QRect figureRectOnField = boundedByFieldBoundedRectByFigure(figureIndex, figurePos, figureRotation);
    for(int x = figureRectOnField.left(); x <= figureRectOnField.right(); ++x)
    {
        for(int y = figureRectOnField.top(); y <= figureRectOnField.bottom(); ++y)
        {
            if(figures[figureIndex][figureRotation][x - figurePos.x()][y - figurePos.y()])
                field[x][y] = figureColor;
        }
    }
}

//----------------------------------------------------------------------
QRect Tetris::boundedByFieldBoundedRectByFigure(int figureIndex, const QPoint &pos, int rotation)
{
    QRect result = boundedFigureRect(figureIndex, rotation);
    result.moveLeft(pos.x() + result.left());
    result.moveTop(pos.y() + result.top());

    return boundedByFieldRect(result);
}

//----------------------------------------------------------------------
QRect Tetris::boundedByFieldRect(QRect rect)
{
    if(rect.left() < 0)
        rect.setLeft(0);
    if(rect.right() >= fieldWidth)
        rect.setRight(fieldWidth - 1);
    if(rect.top() < 0)
        rect.setTop(0);
    if(rect.bottom() >= fieldHeight)
        rect.setBottom(fieldHeight - 1);

    return rect;
}
