#ifndef TETRIS_H
#define TETRIS_H

#include "figures.h"

#include <QPoint>
#include <QColor>


//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
enum class Action : char
{
    Nothing,
    Rotate,
    MoveLeft,
    MoveRight,
    Drop
};


//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
struct Tetris
{
    static constexpr int fieldWidth = 14;
    static constexpr int fieldHeight = 25;
    int field[fieldWidth][fieldHeight];

    Tetris();

    void restart();

    void step();

    void clearField();
    void makeNewFigure();

    bool isCollide(const QPoint &figurePos, int figureRotation) const;
    void deleteFullLines();
    static void writeFigureToField(int field[fieldWidth][fieldHeight], int figureIndex, const QPoint &figurePos, int figureRotation, int figureColor);
    static QRect boundedByFieldBoundedRectByFigure(int figureIndex, const QPoint &pos, int rotation);
    static QRect boundedByFieldRect(QRect rect);

    int figureIndex = 0;
    QPoint figurePos;
    int figureRotation = 0;
    QColor figureColor;
    bool isFirstFigureStep = true;
    bool isFigurePlaced = false;

    int skipFallings = 32;
    int skippedFallings = 0;

    Action action;

    int score = 0;
};

#endif // TETRIS_H
